<?php



/**
 * Pluriels, singulier ou zéro...
 *
 * Les items sont à indiquer comme pour la fonction _T() sous la forme :
 * "module:chaine"
 *
 * Cherchera chaine_zero, chaine_un, chaine_nb en fonction de $nb.
 *
 * @example
 *     ```
 *     [(#NB|forum_pluraliser{forum:sujets})]
 *     [(#NB|forum_pluraliser{#LISTE{forum:sujets_zero,forum:sujets_un,forum:sujets_nb}})]
 *     ```
 *
 * @param int $nb : le nombre
 * @param string|array $chaine
 *     - string : radical de l'item de langue, sera complété par _zero, _un, _nb
 *     - array : couples {0 => module:chaine_zero, 1 => module:chaine_un, 2 => module:chaine_nb}
 *
 * @param array $vars : Les autres variables nécessaires aux chaines de langues (facultatif)
 * @return string : la chaine de langue finale en utilisant la fonction _T()
 */
function forum_pluraliser($nb, $chaine, $vars = array()) {
	$nb = intval($nb);

	if (!is_array($vars)) {
		$vars = array();
	}

	if (!is_array($chaine)) {
		$chaine = array(
			$chaine . '_zero',
			$chaine . '_un',
			$chaine . '_nb',
		);
	}

	$vars['nb'] = $nb;
	if ($nb > 1) {
		return _T($chaine[2], $vars);
	} elseif ($nb == 1) {
		return _T($chaine[1], $vars);
	} else {
		return _T($chaine[0], $vars);
	}
}

/**
 * Retourne la classe CSS de la catégorie (article)
 *
 * Prend le premier chiffre du numéro de titre
 *
 * @param int $id_article
 * @return string classe css tel que 'c1'
 */
function forum_css_couleur($id_article) {
	static $articles = null;
	if (is_null($articles)) {
		$articles = forum_titre_categorie('', true);
		$articles = array_map(
			function($i) {
				return substr(intval(recuperer_numero($i)), 0, 1);
			},
			$articles
		);
	}
	if (isset($articles[$id_article])) {
		return 'c' . $articles[$id_article];
	}
	return '';
}

/**
 * Retourne le titre de la catégorie (article)
 *
 * @param int $id_article
 * @param bool $getall : true pour retourner la liste id_article => titre
 * @return string titre
 */
function forum_titre_categorie($id_article, $getall = false) {
	static $articles = null;
	if (is_null($articles)) {
		$articles = sql_allfetsel('id_article, titre', 'spip_articles');
		$articles = array_column($articles, 'titre', 'id_article');
	}
	if ($getall) {
		return $articles;
	}
	if (isset($articles[$id_article])) {
		return supprimer_numero(typo($articles[$id_article]));
	}
	return '';
}

/**
 * Retourne la classe CSS du groupe de mot
 *
 * @param int $id_groupe_mot
 * @return string classe css tel que 'c1'
 */
function forum_css_couleur_groupe($id_groupe_mot) {
	static $groupes = array(
		3 => 1,
		4 => 2,
		5 => 3,
		6 => 4,
		7 => 0, // admin (résolu, cloturé)
	);
	if (isset($groupes[$id_groupe_mot])) {
		return 'c' . $groupes[$id_groupe_mot];
	}
	return '';
}

/**
 * Compile le critère `{resolus}`
 *
 * Ce critère test si un message fils possède le mot clé résolu (1332)
 *
 * @example
 *     ```
 *     <BOUCLE_(FORUMS){resolus}>  ...
 *     ```
 *
 * @param string $idb Identifiant de la boucle
 * @param array $boucles AST du squelette
 * @param Critere $crit Paramètres du critère dans cette boucle
 * @return void
 */
function critere_FORUMS_resolus_dist($idb, &$boucles, $crit) {
	$boucle = &$boucles[$idb];

	$id_parent = isset($GLOBALS['exceptions_des_tables'][$boucle->id_table]['id_parent']) ?
		$GLOBALS['exceptions_des_tables'][$boucle->id_table]['id_parent'] :
		'id_parent';

	$id_table = $boucle->id_table;
	$id_mot = 1332;

	$boucle->from['resolus'] = 'spip_forum';
	$boucle->join["resolus"] = array("'$id_table'", "'$id_parent'", "'id_forum'", "'resolus.statut='.sql_quote('publie')");
	$boucle->from['mots_liens'] = 'spip_mots_liens';
	$boucle->join["mots_liens"] = array("'resolus'", "'id_objet'", "'id_forum'", "'mots_liens.id_mot=$id_mot AND mots_liens.objet ='.sql_quote('forum')");
	$boucle->select[] = "mots_liens.id_mot AS id_mot";
}

function forum_date_diff_jours($date1, $date2) {
	$d1 = new DateTime($date1);
	$d2 = new DateTime($date2);
	$difference = $d1->diff($d2);
	return $difference->days;
}

/**
 * Échapper les balises HTML d’un contenu, sauf pour les balises `<code>`
 * dont le contenu est déjà échappé.
 *
 * @param $texte
 * @return mixed|string
 */
function echapper_tags_hors_code($texte) {
	$regex = ',<code[^>]*>(.*)</code>,UimsS';
	$echappements = [];
	if (preg_match_all($regex, $texte, $codes, PREG_SET_ORDER)) {
		foreach ($codes as $i => $code) {
			$cle = '%%%' . $i . '%%%';
			$texte = str_replace($code[0], $cle, $texte);
			$echappements[$cle] = $code[0];
		}
	}
	$texte = echapper_tags($texte);
	if (count($echappements)) {
		$texte = str_replace(array_keys($echappements), array_values($echappements), $texte);
	}
	return $texte;
}

/**
 * Lors de l’édition d’un auteur, invalider le cache
 *
 * @param array $flux
 * @return array
 */
function galactic_forum_post_edition($flux) {
	if (
		!empty($flux['args']['table'])
		and !empty($flux['args']['action'])
		and !empty($flux['args']['id_objet'])
		and $flux['args']['table'] == 'spip_auteurs'
		and $flux['args']['action'] == 'modifier'
	) {
		$id_auteur = $flux['args']['id_objet'];
		include_spip('inc/invalideur');
		suivre_invalideur("id='id_auteur/a$id_auteur'");
	}
	return $flux;
}


// moderer les messages depuis l'espace public
// ne supprime pas le message ni le fil mais les passe en 'off'
// on peut toujours les revalider dans l'espace prive
function invalider_forum($id_forum, $r = '') {
	include_spip("inc/securiser_action");
	list($id_auteur, $pass) = caracteriser_auteur();
	$arg = "$id_forum";
	$hash = _action_auteur("instituer_forum-$arg-off", $id_auteur, $pass, 'alea_ephemere');
	$r = rawurlencode(_request('redirect'));
	return generer_url_action('instituer_forum', "arg=$arg-off&hash=$hash&redirect=$r");
	include_spip('inc/invalideur');
	suivre_invalideur("id='id_forum/a$id_article'");
}

function spam_forum($id_forum, $r = '') {
	include_spip("inc/securiser_action");
	list($id_auteur, $pass) = caracteriser_auteur();
	$arg = "$id_forum";
	$hash = _action_auteur("instituer_forum-$arg-spam", $id_auteur, $pass, 'alea_ephemere');
	$r = rawurlencode(_request('redirect'));
	return generer_url_action('instituer_forum', "arg=$arg-spam&hash=$hash&redirect=$r");
	include_spip('inc/invalideur');
	suivre_invalideur("id='id_forum/a$id_article'");
}