### Volonté

Mettre plus en avant la notion de FAQ, en mettant en avant le système de vote,
notamment en permettant de voter les réponses. En gros s’approcher un peu d’un 
fonctionnement à la StackOverFlow, simplifié tout de même.


### Vue d’un sujet

- Permettre de mettre un mot clé "Réponse pertinente" ?
  enfin qqc dans le style, car le mot clé "résolu" ne correspond pas
  au message qui contient la résolution, mais souvent au remerciement du
  rédacteur du sujet (sauf s’il trouve lui-même la réponse).

### Ergonomie

- Déplacer le texte d’intro en colonne et/ou le cacher pour les personnes authentifiées.
- Proposer d’ouvrir un sujet sur la home, en sélectionnant la thématique (l’article)

### Multilinguisme

- Langue des notifications incorrecte ?
- Langue des boutons d’admins incorrects ?
- Impossibilité de changer la langue d’interface dans le privé ?
- Traductions sur trad.spip.net ?

### Listes diverses

- Meilleur affichage en mobile

### Éventuellement ?

- Permettre des petites réponses aux réponses (réponses de niveau 2 donc) ?
- Permettre de lister les sujets non résolus ?