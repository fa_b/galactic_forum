<?php

/***************************************************************************\
 *  SPIP, Systeme de publication pour l'internet                           *
 *                                                                         *
 *  Copyright (c) 2001-2017                                                *
 *  Arnaud Martin, Antoine Pitrou, Philippe Riviere, Emmanuel Saint-James  *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\***************************************************************************/

/**
 * Action pour dissocier un lien entre 2 objets
 *
 * @package SPIP\Core\Liens\API
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/**
 * Action pour dissocier 2 objets entre eux
 *
 * L'argument attendu est `objet1-id1-objet2-id2` (type d'objet, identifiant)
 * tel que `mot-7-rubrique-3`.
 *
 * @uses action_supprimer_lien_dist()
 *
 * @param null|string $arg
 *     Clé des arguments. En absence utilise l'argument
 *     de l'action sécurisée.
 * @return void
 */
function action_supprimer_lien_et_cache_dist($arg = null) {

	if (is_null($arg)) {
		$securiser_action = charger_fonction('securiser_action', 'inc');
		$arg = $securiser_action();
	}
	$supprimer_lien = charger_fonction('supprimer_lien', 'action');
	$supprimer_lien($arg);

	$arg = explode("-", $arg);
	list($objet_source, $ids, $objet_lie, $idl) = $arg;

	include_spip('inc/invalideur');
	suivre_invalideur("id='" . $objet_source . '/' . $ids . "'");
	suivre_invalideur("id='" . $objet_lie . '/' . $idl . "'");
}
