<?php

function formulaires_ajouter_mots_forum_charger_dist($id_forum) {
	if (!$id_forum) {
		return false;
	}
	$valeurs = array(
		'id_forum' => $id_forum,
		'editable' => true,
	);
	return $valeurs;
}

function formulaires_ajouter_mots_forum_verifier_dist($id_forum) {
	$erreurs = array();
	return $erreurs;
}

function formulaires_ajouter_mots_forum_traiter_dist($id_forum) {
	$res = array(
		'editable' => false,
		'message_ok' => _T('info_modification_enregistree'),
	);
	include_spip('action/editer_liens');
	$mots = _request('mots');
	$mots = array_filter($mots);
	$mots = array_map('intval', $mots);
	objet_associer(
		array('mot' => $mots),
		array('forum' => $id_forum)
	);
	include_spip('inc/invalideur');
	suivre_invalideur("id='forum/" . $id_forum .  "'");
	if ($res['message_ok']) {
		// autoclose + reload liste des tags
		$res['message_ok'] .= '<script type="text/javascript">if (window.jQuery) { jQuery.modalboxclose(); ajaxReload("tags"); }</script>';
	}
	return $res;
}
